import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TranslateModule} from '@ngx-translate/core';

import {PageNotFoundComponent} from './components/';
import {WebviewDirective} from './directives/';
import {LoadingComponent} from './components/loading/loading.component';
import { ClarityModule } from '@clr/angular';

@NgModule({
	declarations: [PageNotFoundComponent, WebviewDirective, LoadingComponent],
	imports: [CommonModule, TranslateModule, ClarityModule],
	exports: [TranslateModule, WebviewDirective, LoadingComponent, ClarityModule]
})
export class SharedModule {
}
